'use strict';

/**
 * slider-inicio service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::slider-inicio.slider-inicio');
