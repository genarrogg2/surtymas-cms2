module.exports = ({ env }) => ({
    upload: {
        config: {
            provider: "cloudinary",
            providerOptions: {
                cloud_name: env("CLOUDINARY_NAME"),
                api_key: env("CLOUDINARY_KEY"),
                api_secret: env("CLOUDINARY_SECRET"),
            },
            actionOptions: {
                upload: {},
                delete: {},
            },
        },
    },

    "strapi-chatgpt": {
        enabled: true,
    },

    scheduler: {
        enabled: true,
        config: {
            contentTypes: {
                'api::page.page': {}
            }
        }
    },
    
});
