/**
 * This file was automatically generated by Strapi.
 * Any modifications made will be discarded.
 */
import ckeditor5 from "@_sh/strapi-plugin-ckeditor/strapi-admin";
import apolloSandbox from "@creazy231/strapi-plugin-apollo-sandbox/strapi-admin";
import calendar from "@offset-dev/strapi-calendar/strapi-admin";
import strapiAppVersion from "@palmabit/strapi-app-version/strapi-admin";
import strapiCloud from "@strapi/plugin-cloud/strapi-admin";
import colorPicker from "@strapi/plugin-color-picker/strapi-admin";
import graphql from "@strapi/plugin-graphql/strapi-admin";
import i18N from "@strapi/plugin-i18n/strapi-admin";
import usersPermissions from "@strapi/plugin-users-permissions/strapi-admin";
import scheduler from "@webbio/strapi-plugin-scheduler/strapi-admin";
import strapiChatgpt from "strapi-chatgpt/strapi-admin";
import multiSelect from "strapi-plugin-multi-select/strapi-admin";
import restCache from "strapi-plugin-rest-cache/strapi-admin";
import { renderAdmin } from "@strapi/strapi/admin";

renderAdmin(document.getElementById("strapi"), {
  plugins: {
    ckeditor5: ckeditor5,
    "apollo-sandbox": apolloSandbox,
    calendar: calendar,
    "strapi-app-version": strapiAppVersion,
    "strapi-cloud": strapiCloud,
    "color-picker": colorPicker,
    graphql: graphql,
    i18n: i18N,
    "users-permissions": usersPermissions,
    scheduler: scheduler,
    "strapi-chatgpt": strapiChatgpt,
    "multi-select": multiSelect,
    "rest-cache": restCache,
  },
});
